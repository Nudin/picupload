#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import exifread
import glob
from sys import argv

"""1. Zeichen"""
blatt_form_nervatur = {
"_": "keine Blätter (z.B. Schmarotzer, Kakteen)",
"f": "Blattnerven fehlen (sind nicht erkennbar)",
"h": "nur Hauptnerv erkennbar",
"n": "Blätter nadelartig, zapfenartig",
"p": "Blattnerven parallel",
"s": "Blätter schlauchartig",
"v": "Blattnerven hierarisch verzweigend",
"w": "Blattnerven netzartig, mosaikartig" }

"""2. Zeichen"""
blueten_stand_symmetrie = {
"0": u"Einzelblüte mit nicht erkennbarer Symmetrie, da Blüte zu klein oder keine Kronblätter vorhanden sind",
"1": u"zygomorphe Einzelblüte ( höchstens eine Symmetrieachse )",
"2": u"zygomorphe Blüten an rispen- trauben- zylinder- kegel-, kugelförmigem oder ähnlichem Blütenstand",
"3": u"zygomorphe Blüten  an doldenähnlichem Blütenstand",
"4": u"radiäre Einzelblüte ( mehr als eine Symmetrieachse )",
"5": u"radiäre Blüten an rispen- trauben- zvlinder- kegel-, kugelförmigem oder ähnlichem Blütenstand",
"6": u"radiäre Blüten an doldenähnlichem Blütenstand",
"7": u"kopf- oder körbchenähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten, der durch vom Korb abgesetzte, meist andersfarbige Zungenblüten, wie eine einzige Blüte aussieht",
"8": u"kopf- oder körbchenähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten, ohne auf den ersten Blick erkennbare Zungenblüten",
"9": u"kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten oder kugel-, kopf- oder körbchen-ähnlicher Blütenstand mit zygomorphen oder radiären Einzelblüten" }

"""3. Zeichen
0-6 0   Zahl der Blütenblätter nicht erkennbar
1-6 1   nur 1 Blütenblatt bzw verwachsene Blütenblätter, die wie ein Blütenblatt oder eine Röhre wirken
1-3 2-7 zygomorphe Blüten mit genau 2, 3, 4, 5, 6 oder 7 und mehr Blütenblättern
4-6 2-7 radiäre Blüten mit genau 2, 3, 4, 5, 6 oder 7 und mehr Blütenblättern
7-8 1   isolierter kopf- oder körbchenähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten,
7-8 2   mehrere kopf- oder körbchenähnliche Blütenstände bilden eine Dolde oder Rispe
9   0   kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten
9   1   kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand  mit sehr kleinen Blüten und mit Blütenblatt-ähnlichem Hüllblatt oder Hüllblättern
9   2   kopf- oder körbchen-ähnlicher Blütenstand mit zygomorphen Blüten
9   5   kopf- oder körbchen-ähnlicher Blütenstand mit radiären Blüten
"""
bluetenblaetter = {
    "[0-6]": {
        "0": u"Zahl der Blütenblätter nicht erkennbar"
    },
    "[1-6]": {
        "1": u"nur 1 Blütenblatt bzw verwachsene Blütenblätter, die wie ein Blütenblatt oder eine Röhre wirken"
    },
    "[1-3]": {
        "2": u"zygomorphe Blüten mit genau 2 Blütenblättern",
        "3": u"zygomorphe Blüten mit genau 3 Blütenblättern",
        "4": u"zygomorphe Blüten mit genau 4 Blütenblättern",
        "5": u"zygomorphe Blüten mit genau 5 Blütenblättern",
        "6": u"zygomorphe Blüten mit genau 6 Blütenblättern",
        "7": u"zygomorphe Blüten mit 7 und mehr Blütenblättern"
    },
    "[4-6]": {
        "2": u"radiäre Blüten mit genau 2 Blütenblättern",
        "3": u"radiäre Blüten mit genau 3 Blütenblättern",
        "4": u"radiäre Blüten mit genau 4 Blütenblättern",
        "5": u"radiäre Blüten mit genau 5 Blütenblättern",
        "6": u"radiäre Blüten mit genau 6 Blütenblättern",
        "7": u"radiäre Blüten mit 7 und mehr Blütenblättern"
    },
    "[7-8]": {
        "1": u"isolierter kopf- oder körbchenähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten,",
        "2": u"mehrere kopf- oder körbchenähnliche Blütenstände bilden eine Dolde oder Rispe",
    },
    "9": {
        "0": u"kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten",
        "1": u"kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand  mit sehr kleinen Blüten und mit Blütenblatt-ähnlichem Hüllblatt oder Hüllblättern",
        "2": u"kopf- oder körbchen-ähnlicher Blütenstand mit zygomorphen Blüten",
        "5": u"kopf- oder körbchen-ähnlicher Blütenstand mit radiären Blüten",
    },
}

"""4. Zeichen"""
blattrand_d = {
"g": u"glattrandiger Blattrand",
"u": u"unbekannte Art des Blattrandes (teilweise unterschiedlich)",
"z": u"kein Blattrand, da kein Blatt vorhanden ist nadel und schlauch-artige Blätter haben glatten Rand" }

"""5. Zeichen"""
krautig_verholzend = {
"_": u"krautige Pflanzen",
"~": u"verholzende Pflanzen" }

"""6. und 7. Zeichen - Farbcode"""

#def sanitize (in_str:str):
def sanitize (in_str):
    return in_str.strip(' \t\n\0')

class defaultobj:
    printable = ""

def decode_utf16(array):
    s = ''
    for c in [ unichr(array[i]) for i in xrange(0,len(array)) if i % 2 == 0 ]:
        s += c
    return s

def printmeta(filename):
    f = open(filename)
    tags = exifread.process_file(f)
    for tag in tags.keys():
        if tag not in ('JPEGThumbnail', 'TIFFThumbnail'):
            print("\t Key: %s, value: %s" % (tag, tags[tag]))

def recog_num (bluete, zahl):
    #print ("\t" + bluete + ", " + zahl)
    bluete = int(bluete)
    zahl = int(zahl)
    if bluete >= 4 and bluete <=6 and zahl >= 2 and zahl <= 7:
        repl_str = ""
        if zahl < 7:
            repl_str = "genau " + str(zahl)
        else: # 7
            repl_str = "7 und mehr"
        return u"radiäre Blüten mit %s Blütenblättern"% (repl_str)
    elif bluete >= 1 and bluete <=3 and zahl >= 2 and zahl <= 7:
        repl_str = ""
        if zahl < 7:
            repl_str = "genau " + str(zahl)
        else: # 7
            repl_str = "7 und mehr"
        return u"zygomorphe Blüten mit %s Blütenblättern"% (repl_str)
    elif bluete >= 1 and bluete <=6 and zahl == 1:
        return u"nur 1 Blütenblatt bzw verwachsene Blütenblätter, die wie ein Blütenblatt oder eine Röhre wirken"
    elif bluete >= 0 and bluete <=6 and zahl == 0:
        return u"Zahl der Blütenblätter nicht erkennbar"
    elif bluete >= 7 and bluete <=8:
        if zahl == 1:
            return u"isolierter kopf- oder körbchenähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten,"
        if zahl == 2:
            return u"mehrere kopf- oder körbchenähnliche Blütenstände bilden eine Dolde oder Rispe"
    elif bluete == 9:
        if zahl == 0:
            return u"kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand mit röhrenförmigen oder sehr kleinen Blüten"
        if zahl == 1:
            return u"kugel-, kegel-, trauben- oder zylinderähnlicher Blütenstand  mit sehr kleinen Blüten und mit Blütenblatt-ähnlichem Hüllblatt oder Hüllblättern"
        if zahl == 2:
            return u"kopf- oder körbchen-ähnlicher Blütenstand mit zygomorphen Blüten"
        if zahl == 5:
            return u"kopf- oder körbchen-ähnlicher Blütenstand mit radiären Blüten"
    else:
        print ("\tPrasing_error")
        raise Exception("Parsing_error")

def extractmeta(filename):
    f = open(filename)
    tags = exifread.process_file(f)
    f.close()
    ret = {}
    author = tags['Image XPAuthor'].printable
    try:
        code = tags['Image Copyright'].values
        ret['code'] = sanitize(code)
    except:
        code = ""
    try:
        date = tags['EXIF DateTimeOriginal'].printable
    except:
        date = tags.get('Image DateTime', defaultobj ).printable
    ret['date'] = sanitize (date)
    desccode = tags.get('Image ImageDescription', defaultobj).printable
    try:
        lat_name_coded = tags['Image XPSubject'].values
        lat_name = decode_utf16(lat_name_coded)
        ret['lat_name'] = sanitize (lat_name)
    except:
        lat_name = ""
    try:
        place_coded = tags['Image XPKeywords'].values
        place = decode_utf16(place_coded)
        ret['place'] = sanitize (place)
    except:
        place = ""
    #['Image XPTitle'].values # desc-code
    try:
        flora_coded = tags['Image XPComment'].values ## ???
        flora = decode_utf16(flora_coded)
        ret['flora'] = sanitize (flora)
    except:
        flora = ""
    #print(author)
    #print(code)
    #print(date)
    #print("desc"+desccode)
    print("\t" + lat_name)
    if (desccode != ""):
        print ("\t" + desccode + ", " + str(len (desccode)))
        try:
            blatt  = blatt_form_nervatur[desccode[0]]
            ret['blatt'] = sanitize (blatt)
        except:
            print("\tmeep - blatt")
        try:
            bluete = blueten_stand_symmetrie[desccode[1]]
            print ("\t" + bluete)
            ret['bluete'] = sanitize (bluete)
            print ("\t" + ret['bluete'])
        except:
            print("\tmeep - bluete")
        try:
            numblueten = recog_num(desccode[1], desccode[2]) ## Error!?
            ret['numblueten'] = sanitize (numblueten)
        except:
            print("\tmeep - numblueten")
        try:
            blattrand  = blattrand_d[desccode[3]]
            ret['blattrand'] = sanitize (blattrand)
        except:
            print("\tmeep - blattrand")
        try:
            holz  = krautig_verholzend[desccode[4]]
            ret['holz'] = sanitize (holz)
        except:
            print("\tmeep - holz")
        try:
            color = desccode[5:7]
            ret['color'] = sanitize (color)
        except:
            print("\tmeep - color")
        try:
            print ("\t" + blatt + bluete + numblueten + blattrand + holz + color)
        except:
            print("\tmeep")
        #except:
        #    print("meep")
    #print(place)
    #print(flora)
    return ret


for filename in glob.iglob(argv[1] + '[1-5]*_*/*.JPG'):
    print("== " + filename + " ==")
    try:
        descr = extractmeta(filename)
    except Exception as error:
        print("\tProblem:" + error)
