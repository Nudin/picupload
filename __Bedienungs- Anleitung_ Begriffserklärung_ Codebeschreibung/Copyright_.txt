Copyright

Die meisten vorhandenen Fotos wurden von mir selbst erstellt. 
Einige Fotos stammen von meiner Lebensgefaehrtin Grazyna Malicki 
und ein paar habe ich "Pixabay" und "Smagy" entnommen.
Die jeweilige Herkunft des Bildes findet man unter "Autoren" in der Exif-Datei.

Die Bilder k�nnen ohne Angabe des Urhebers kopiert, ver�ndert  und weitergegeben werden.
Durch diese Freistellung ist aber selbst den Eigent�mern ein Verkauf der Bildrechte nicht mehr m�glich.
Falls ein ver�ndertes Bild verkauft werden soll, mu� der K�ufer auf obigen Zusammenhang aufmerksam gemacht werden.
 
									 Ulrich Heinersdorff

Fehlerberichtigungen, Fragen, Anregungen und Hinweise bitte an  ulrich_heinersdorff@freenet.de   
